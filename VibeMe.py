#!/usr/bin/env python
# -*- coding: cp1252 -*-
"""Controls a Europe Magic Wand"""

import sys
import RPi.GPIO as GPIO
from random import randint
import memcache
from time import sleep
from time import time
import subprocess  # only needed, if you want to use a 16x2 LCD Display for display of Ch A, Ch B and the mode you are in

# first install python memcached plugin

mc = memcache.Client(['127.0.0.1:11211'], debug=0)

# Define, what functions to call on startup, according to scriptname
# This technique is used to be interoperable with pyLauncher (v 1.1.1)
action = sys.argv[0]
action = action.split("/")[-1].split(".")[0]  # getting the calling script name

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)   # Numbers GPIOs by physical location

# Define Pin Numbers
on  = 8    # turns Wand on
off = 10   # turns Wand off


GPIO.setup(on, GPIO.OUT)   # Set on  PIN mode to output
GPIO.setup(off, GPIO.OUT)  # Set off PIN mode to output


# Electricity off - LEDS/PINS off

GPIO.output(on,  GPIO.LOW)
GPIO.output(off, GPIO.LOW)


## Defining functions

def quickpress(button, t=0.25, w=1 ):  # t=how long it's pressed, w=how often, default 1
    """Quickly presses and releases a button"""
    x = 0  # counter
 
    while x < w:

        GPIO.output(button, GPIO.HIGH)  # Press button
        sleep(t)

        GPIO.output(button, GPIO.LOW)  # Release button
        sleep(t)
        x += 1

def destroy():
    GPIO.setmode(GPIO.BOARD)   # Numbers GPIOs by physical location
    GPIO.setwarnings(False)
    GPIO.cleanup()             # Release resource

def up ():
    quickpress(on)

def down ():
    quickpress(off)

def out ():
    quickpress(off)
    quickpress(off)
    quickpress(off)
    quickpress(off)
    quickpress(off)
    quickpress(off)


def set_orgasm ():
    quickpress(on)
    quickpress(on)
    quickpress(on)
    quickpress(on)
    quickpress(on)
    quickpress(on)
    
def ride(duration=10):
    """if heartbeat under 75 - Wand on
       if hearbeat over 100 - Wand off :-)  """

    starttime = int(time())
    now = starttime
    turn_on_time = starttime
    counter = 0
    up()
    sleep (5)
    low = 73
    high = 80
    off = 0

    while int(time()) < starttime + duration*60:
        B = int(mc.get("heartbeat"))
        print B
        now = int(time())

        if B <= low and now > turn_on_time + 15:     # get the party started
            up()
            turn_on_time = int(time())
            
        elif B >= high:   # turn off, don't get too excited
            out()
            off = 1
        elif B > low and B < high:
            if now > turn_on_time + 30 and not off:
                print "no change after 30 secs - turning up"
                up()
                turn_on_time = int(time())

        sleep (2)
        counter+=1
        if counter%30 == 0:
            print "raising the stakes"
            low+=1
            high+=1

    print "stopping the ride"
    out()

## Functions End



## Main programm starts here
if __name__ == '__main__':     # Program start from here

    try:
        
        if action == "up": # turn up 
            up()

        elif action == "down": # turn down
            down()

        elif action == "off":
            out()

        elif action == "orgsm":
            set_orgasm()

        elif action == "ten":
            ride(10)

        elif action == "twnty":
            ride(20)

        elif action == "five":
            ride(5)


        destroy()
        exit(0)

    except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
        out()
        destroy()
