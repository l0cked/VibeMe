pyLauncher:    by LittleBytesOfPi.com
version: 1.1.1

Sourcecode: https://bitbucket.org/gbriggs/lbp_tcpipsockets

Get the App from the Google Play Store:
  https://play.google.com/store/search?q=pyLauncher&c=apps

#########################################################
# Due to a bug, put the whole project in folder /vibe  ##
# to make sure, you see all the commands in the app    ##
#########################################################
