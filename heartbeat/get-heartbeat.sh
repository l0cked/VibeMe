#!/bin/bash
service bluetooth stop
rfkill unblock "bluetooth" # override Bluetooth off
sleep 1
hciconfig hci0 up
sleep 1
gatttool -b 00:22:D0:B2:E6:46 --char-write-req \
	--handle=0x0013 --value=0100 --listen | perl -ne '

BEGIN {
	use Cache::Memcached;

	$memd = new Cache::Memcached { 
	    'servers' => ["127.0.0.1:11211"], 
            'debug' => 0 
	};
      }

if(/.*value: (\w+) (\w+) (\w+) (\w+)/) { 
    ($x, $y, $z, $a) = ($1, $2, $3, $4);
    $rr = hex("$a$z"); 
    printf ("%d, %f, %x, %x, %d\n", hex($y), $rr/1024, $a, $z, $rr);
    $memd -> set('heartbeat', hex($y), 3);  # only valid for 3 seconds 
}
'
